from tkinter import *

class Sudoku:

    row = ""
    col = ""

    def void(self):
        estructura = self.cargar_sudoku()
        raiz = self.interfaz()
        frame = self.frame()
        # label_title = Label(frame, text="Taller Sudoku")
        # label_title.place(x = 280 , y = 5)
        self.sudoku(estructura,frame)
        # sudoku = Entry(Frame)
        # sudoku.grid()

        raiz.mainloop()
        # print(estructura)
    
    def cargar_sudoku(self):
        sudoku_txt = open("sudoku.txt", "r")
        sudoku = []
        for linea in sudoku_txt.read():
            sudoku.append(linea) 
        sudoku_txt.close()
        estruc_sudoku1 = []
        for f in sudoku:
            if f != "," :
                estruc_sudoku1.append(f)
        estruc_sudoku2 = []
        for f in estruc_sudoku1:
            if f != "\n":
                estruc_sudoku2.append(f)
        return estruc_sudoku2

    def interfaz(self):
        raiz = Tk()
        raiz.title("Sudoku")
        raiz.resizable(1,1)
        raiz.iconbitmap("sudokugame.ico")
        return raiz
    
    def frame(self):
        frame = Frame()
        frame.pack(fill="both", expand="True")
        frame.config(bg="grey")
        frame.config(width="650", height="350")
        frame.config(cursor="hand2")
        return frame
    
    def sudoku(self, sudoku, frame):
        frame.pack()
        count = 0
        for r in range(0, 9):
            for c in range(0, 9):
                print(sudoku[count])
                if sudoku[count] != 0:
                    cell = Label(frame,text=sudoku[count], width=10)
                    cell.grid(padx=5, pady=5, row=r, column=c)
                    count = count + 1
                else:
                    cell = Label(frame, width=10)
                    cell.grid(padx=5, pady=5, row=r, column=c)
                    count = count + 1
                



main = Sudoku()
main.void()